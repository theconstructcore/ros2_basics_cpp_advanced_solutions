cmake_minimum_required(VERSION 3.8)
project(qos_exercises_pkg)

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

# find dependencies
find_package(ament_cmake REQUIRED)
find_package(rclcpp REQUIRED)
find_package(std_msgs REQUIRED)


if(BUILD_TESTING)
  find_package(ament_lint_auto REQUIRED)
  ament_lint_auto_find_test_dependencies()
endif()

# Build
add_executable(simple_qos_example_1_node src/simple_qos_example_1.cpp)
ament_target_dependencies(simple_qos_example_1_node rclcpp std_msgs)

add_executable(simple_qos_example_2_node src/simple_qos_example_2.cpp)
ament_target_dependencies(simple_qos_example_2_node rclcpp std_msgs)


# Install 
install(TARGETS
simple_qos_example_1_node
simple_qos_example_2_node
   DESTINATION lib/${PROJECT_NAME}
 )

ament_package()

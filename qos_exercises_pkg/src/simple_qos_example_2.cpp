#include <gtest/gtest.h>
#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/string.hpp>

#include <rmw/qos_profiles.h>

// Node::create_subscription(
//   const std::string & topic_name,
//   const rclcpp::QoS & qos,
//   CallbackT && callback,
//   const SubscriptionOptionsWithAllocator<AllocatorT> & options,
//   typename MessageMemoryStrategyT::SharedPtr msg_mem_strat)
// {
//   return rclcpp::create_subscription<MessageT>(
//     *this,
//     extend_name_with_sub_namespace(topic_name, this->get_sub_namespace()),
//     qos,
//     std::forward<CallbackT>(callback),
//     options,
//     msg_mem_strat);
// }

class SubscriberDDS : public rclcpp::Node {
public:
  SubscriberDDS(std::string topic_name) : Node("simple_subscriber") {

    // Directly setting only the queue
    subscription_ = this->create_subscription<std_msgs::msg::String>(
        topic_name, rclcpp::QoS(10),
        std::bind(&SubscriberDDS::topic_callback, this, std::placeholders::_1));
  }

private:
  void topic_callback(const std_msgs::msg::String::SharedPtr msg) {
    RCLCPP_INFO(this->get_logger(), "String Message=['%s']", msg->data.c_str());

    std::string s = msg->data;
    std::string delimiter = ":";
    this->id = stoi(s.substr(0, s.find(delimiter)));

    std::string time_data = s.substr(1, s.find(delimiter));
    std::string delimiter_time = ",";
    int seconds = stoi(time_data.substr(0, time_data.find(delimiter_time)));
    int nano_seconds =
        stoi(time_data.substr(1, time_data.find(delimiter_time)));

    if (this->id == 0) {
      this->id_next = 0;
    }

    if (this->id == this->id_next) {
      RCLCPP_INFO(this->get_logger(),
                  "MESSAGE OK: ID=%i, Next Id=%i, Total MSG lost=%i", this->id,
                  this->id_next, this->lost_messages_counter);
    } else {
      if (this->id_next != -1) {
        int delta_messages_lost = this->id - this->id_next;
        this->lost_messages_counter += delta_messages_lost;
        RCLCPP_INFO(
            this->get_logger(),
            "Message LOST: ID=%i, Next Id=%i, DELTA MSG LOST=%i, Total MSG "
            "lost=%i",
            this->id, this->id_next, delta_messages_lost,
            this->lost_messages_counter);

      } else {
        // Nothing
      }
    }

    this->id_next = this->id + 1;

    rclcpp::Time time_obj(seconds, nano_seconds, RCL_ROS_TIME);
    // auto now_time = this->get_clock().to_msg();

    auto now_time = this->get_clock()->now();

    rclcpp::Duration delta = time_obj - time_obj;
  }

  rclcpp::Subscription<std_msgs::msg::String>::SharedPtr subscription_;

  int id = 0;
  int id_next = -1;
  int lost_messages_counter = 0;
};

int main(int argc, char *argv[]) {
  // Some initialization.
  rclcpp::init(argc, argv);

  // Instantiate a node.
  std::string topic_name_in = "/dds_test";
  std::shared_ptr<SubscriberDDS> node =
      std::make_shared<SubscriberDDS>(topic_name_in);

  // Start and spin executor SingleThreadded
  rclcpp::spin(node);

  // Shutdown and exit.
  rclcpp::shutdown();
  return 0;
}
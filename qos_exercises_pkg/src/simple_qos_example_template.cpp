#include <gtest/gtest.h>
#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/string.hpp>

#include <rmw/qos_profiles.h>

// Node::create_subscription(
//   const std::string & topic_name,
//   const rclcpp::QoS & qos,
//   CallbackT && callback,
//   const SubscriptionOptionsWithAllocator<AllocatorT> & options,
//   typename MessageMemoryStrategyT::SharedPtr msg_mem_strat)
// {
//   return rclcpp::create_subscription<MessageT>(
//     *this,
//     extend_name_with_sub_namespace(topic_name, this->get_sub_namespace()),
//     qos,
//     std::forward<CallbackT>(callback),
//     options,
//     msg_mem_strat);
// }

class SubscriberDDS : public rclcpp::Node {
public:
  SubscriberDDS(std::string topic_name) : Node("simple_subscriber") {

    // Directly setting only the queue
    subscription_ = this->create_subscription<std_msgs::msg::String>(
        topic_name, 10,
        std::bind(&SubscriberDDS::topic_callback, this, std::placeholders::_1));

    // We set using the QoS Class:
    // https://docs.ros2.org/galactic/api/rclcpp/classrclcpp_1_1QoS.html#a98fb6b31d7c5cbd4788412663fd38cfb
    // https://docs.ros2.org/galactic/api/rmw/qos__profiles_8h_source.html
    // https://docs.ros2.org/galactic/api/rclcpp/qos_8hpp_source.html

    /**
    There some QoS That can be set directly, like the Queue, KeepLAst or KeepAll
    rclcpp::QoS(10)
    rclcpp::QoS(rclcpp::KeepLast(42))
    rclcpp::QoS(rclcpp::KeepAll())

    All the rest have to be set through teh QoS functions or through the rmw
    profiles systems
    **/

    subscription2_ = this->create_subscription<std_msgs::msg::String>(
        topic_name, rclcpp::QoS(10),
        std::bind(&SubscriberDDS::topic_callback, this, std::placeholders::_1));

    // This structure: rclcpp::QoS::QoS 	( 	size_t
    // history_depth
    // )
    size_t custom_history_depth = 10;
    rclcpp::QoS custom_qos_profile(custom_history_depth);

    subscription3_ = this->create_subscription<std_msgs::msg::String>(
        topic_name, custom_qos_profile,
        std::bind(&SubscriberDDS::topic_callback, this, std::placeholders::_1));

    // Using the QoS to set other elements:
    size_t custom_history_depth_2 = 3;
    rclcpp::QoS custom_qos_profile_2(custom_history_depth_2);
    // We are goint to check that those values were set using the methods given
    // here We are using the google test gtest.h We can set

    custom_qos_profile_2.keep_all();
    EXPECT_EQ(rclcpp::HistoryPolicy::KeepAll, custom_qos_profile_2.history());

    custom_qos_profile_2.keep_last(20);
    EXPECT_EQ(rclcpp::HistoryPolicy::KeepLast, custom_qos_profile_2.history());
    EXPECT_EQ(20u, custom_qos_profile_2.depth());

    custom_qos_profile_2.reliable();
    EXPECT_EQ(rclcpp::ReliabilityPolicy::Reliable,
              custom_qos_profile_2.reliability());

    custom_qos_profile_2.reliability(rclcpp::ReliabilityPolicy::BestEffort);
    EXPECT_EQ(rclcpp::ReliabilityPolicy::BestEffort,
              custom_qos_profile_2.reliability());

    custom_qos_profile_2.durability_volatile();
    EXPECT_EQ(rclcpp::DurabilityPolicy::Volatile,
              custom_qos_profile_2.durability());

    custom_qos_profile_2.transient_local();
    EXPECT_EQ(rclcpp::DurabilityPolicy::TransientLocal,
              custom_qos_profile_2.durability());

    custom_qos_profile_2.durability(rclcpp::DurabilityPolicy::Volatile);
    EXPECT_EQ(rclcpp::DurabilityPolicy::Volatile,
              custom_qos_profile_2.durability());

    custom_qos_profile_2.history(RMW_QOS_POLICY_HISTORY_KEEP_ALL);
    EXPECT_EQ(rclcpp::HistoryPolicy::KeepAll, custom_qos_profile_2.history());

    custom_qos_profile_2.history(rclcpp::HistoryPolicy::KeepLast);
    EXPECT_EQ(rclcpp::HistoryPolicy::KeepLast, custom_qos_profile_2.history());

    constexpr rcl_duration_value_t duration_ns = 12345;
    constexpr std::chrono::nanoseconds duration(duration_ns);
    custom_qos_profile_2.deadline(duration);
    EXPECT_EQ(duration_ns, custom_qos_profile_2.deadline().nanoseconds());

    const rmw_time_t rmw_time{0, 54321};
    custom_qos_profile_2.deadline(rmw_time);
    EXPECT_EQ(rmw_time.sec,
              custom_qos_profile_2.get_rmw_qos_profile().deadline.sec);
    EXPECT_EQ(rmw_time.nsec,
              custom_qos_profile_2.get_rmw_qos_profile().deadline.nsec);

    custom_qos_profile_2.lifespan(duration);
    EXPECT_EQ(duration_ns, custom_qos_profile_2.lifespan().nanoseconds());

    custom_qos_profile_2.lifespan(rmw_time);
    EXPECT_EQ(rmw_time.sec,
              custom_qos_profile_2.get_rmw_qos_profile().lifespan.sec);
    EXPECT_EQ(rmw_time.nsec,
              custom_qos_profile_2.get_rmw_qos_profile().lifespan.nsec);

    custom_qos_profile_2.liveliness(RMW_QOS_POLICY_LIVELINESS_MANUAL_BY_TOPIC);
    EXPECT_EQ(rclcpp::LivelinessPolicy::ManualByTopic,
              custom_qos_profile_2.liveliness());

    custom_qos_profile_2.liveliness(rclcpp::LivelinessPolicy::Automatic);
    EXPECT_EQ(rclcpp::LivelinessPolicy::Automatic,
              custom_qos_profile_2.liveliness());

    custom_qos_profile_2.liveliness_lease_duration(duration);
    EXPECT_EQ(duration_ns,
              custom_qos_profile_2.liveliness_lease_duration().nanoseconds());

    custom_qos_profile_2.liveliness_lease_duration(rmw_time);
    EXPECT_EQ(rmw_time.sec, custom_qos_profile_2.get_rmw_qos_profile()
                                .liveliness_lease_duration.sec);
    EXPECT_EQ(rmw_time.nsec, custom_qos_profile_2.get_rmw_qos_profile()
                                 .liveliness_lease_duration.nsec);

    custom_qos_profile_2.avoid_ros_namespace_conventions(true);
    EXPECT_TRUE(custom_qos_profile_2.avoid_ros_namespace_conventions());
    custom_qos_profile_2.avoid_ros_namespace_conventions(false);
    EXPECT_FALSE(custom_qos_profile_2.avoid_ros_namespace_conventions());

    subscription3_ = this->create_subscription<std_msgs::msg::String>(
        topic_name, custom_qos_profile_2,
        std::bind(&SubscriberDDS::topic_callback, this, std::placeholders::_1));

    // We can use also rmw profiles that exist by default or create our own
    // rclcpp::QoS::QoS 	( 	const QoSInitialization &
    // qos_initialization, 	const rmw_qos_profile_t &  	initial_profile
    // = rmw_qos_profile_default
    // )
    // Options available by default:
    // https://docs.ros2.org/galactic/api/rmw/qos__profiles_8h_source.html
    // Options:
    // rmw_qos_profile_sensor_data
    // rmw_qos_profile_parameters
    // rmw_qos_profile_default
    // rmw_qos_profile_services_default
    // rmw_qos_profile_parameter_events
    // rmw_qos_profile_system_default
    // rmw_qos_profile_unknown

    rclcpp::QoS custom_qos_profile_4(
        rclcpp::QoSInitialization::from_rmw(rmw_qos_profile_sensor_data),
        rmw_qos_profile_sensor_data);

    subscription4_ = this->create_subscription<std_msgs::msg::String>(
        topic_name, custom_qos_profile_4,
        std::bind(&SubscriberDDS::topic_callback, this, std::placeholders::_1));

    // Custom rmw profile

    rclcpp::QoS custom_qos_profile_5(
        rclcpp::QoSInitialization::from_rmw(this->my_custom_qos_profile),
        rmw_qos_profile_sensor_data);

    subscription5_ = this->create_subscription<std_msgs::msg::String>(
        topic_name, custom_qos_profile_5,
        std::bind(&SubscriberDDS::topic_callback, this, std::placeholders::_1));
  }

private:
  void topic_callback(const std_msgs::msg::String::SharedPtr msg) {
    RCLCPP_INFO(this->get_logger(), "String Message=['%s']", msg->data.c_str());
  }

  constexpr static const rmw_qos_profile_t my_custom_qos_profile = {
      RMW_QOS_POLICY_HISTORY_KEEP_LAST,
      5,
      RMW_QOS_POLICY_RELIABILITY_RELIABLE,
      RMW_QOS_POLICY_DURABILITY_TRANSIENT_LOCAL,
      RMW_QOS_DEADLINE_DEFAULT,
      RMW_QOS_LIFESPAN_DEFAULT,
      RMW_QOS_POLICY_LIVELINESS_SYSTEM_DEFAULT,
      RMW_QOS_LIVELINESS_LEASE_DURATION_DEFAULT,
      false};

  rclcpp::Subscription<std_msgs::msg::String>::SharedPtr subscription_;
  rclcpp::Subscription<std_msgs::msg::String>::SharedPtr subscription2_;
  rclcpp::Subscription<std_msgs::msg::String>::SharedPtr subscription3_;
  rclcpp::Subscription<std_msgs::msg::String>::SharedPtr subscription4_;
  rclcpp::Subscription<std_msgs::msg::String>::SharedPtr subscription5_;

  int id = 0;
  int id_next = -1;
  int lost_messages_counter = 0;
};

int main(int argc, char *argv[]) {
  // Some initialization.
  rclcpp::init(argc, argv);

  // Instantiate a node.
  std::string topic_name_in = "/dds_test";
  std::shared_ptr<SubscriberDDS> node =
      std::make_shared<SubscriberDDS>(topic_name_in);

  // Start and spin executor SingleThreadded
  rclcpp::spin(node);

  // Shutdown and exit.
  rclcpp::shutdown();
  return 0;
}